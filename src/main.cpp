#include <Arduino.h>
#include "USBHost_t36.h"

USBHost myusb;
USBHub hub1(myusb);
USBHIDParser hid1(myusb);
JoystickController joystick1(myusb);

const int led = LED_BUILTIN;

bool first_joystick_message = false;

int psAxis[64];
uint32_t buttons_prev = 0;
uint32_t buttons;

//=============================================================================
// displayPS4Data
//=============================================================================
void displayPS4Data()
{
  buttons = joystick1.getButtons();
  //TouchscreenUSB.press(count, 500 + 500 * count, 500, 200);
  digitalWrite(led, HIGH);
  if (buttons != buttons_prev) {
      if(buttons == 0x1){//Srq
        TouchscreenUSB.press(1, 5679, 22749, 200);
      } else {
        TouchscreenUSB.release(1);
      }
      if(buttons == 0x2){//Cross
        TouchscreenUSB.press(2, 5679, 26537, 200);
      } else {
        TouchscreenUSB.release(2);
      }
      if(buttons == 0x4){//Circ
        TouchscreenUSB.press(3, 5679, 30326, 200);
      } else {
        TouchscreenUSB.release(3);
      }
      if(buttons == 0x8){//Tri
        TouchscreenUSB.press(4, 5679, 18960, 200);
      } else {
        TouchscreenUSB.release(4);
      }
      buttons_prev = buttons;  
  }


  // stick
  // is the stick outside the middle
  int dx = 127 -  psAxis[0];
  int dy = 127 -  psAxis[1];
  if (dx * dx + dy * dy > 20) { // threshold
    uint32_t sx = 3874; // horizontal
    uint32_t sy = 7017; // vertical

    sx -= dx * 973 / 128;
    sy += dy * 1556 / 128;

    TouchscreenUSB.press(0, sy, sx, 200);
  } else {
    TouchscreenUSB.release(0);
  }
}

void setup() {
    digitalWrite(led, LOW);

  myusb.begin();


}

void loop() {
  // put your main code here, to run repeatedly:

  myusb.Task();

  if (joystick1.available()) {
    if (first_joystick_message) {
      Serial.printf("*** First Joystick message %x:%x ***\n",
                    joystick1.idVendor(), joystick1.idProduct());
      first_joystick_message = false;

      const uint8_t *psz = joystick1.manufacturer();
      if (psz && *psz) Serial.printf("  manufacturer: %s\n", psz);
      psz = joystick1.product();
      if (psz && *psz) Serial.printf("  product: %s\n", psz);
      psz = joystick1.serialNumber();
      if (psz && *psz) Serial.printf("  Serial: %s\n", psz);

      // lets try to reduce number of fields that update
      joystick1.axisChangeNotifyMask(0xFFFFFl);
    }

    for (uint8_t i = 0; i < 64; i++) {
      psAxis[i] = joystick1.getAxis(i);
    }
    displayPS4Data();
  }
  delay(50);
}