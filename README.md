# lanecontroller
Hacky DS4 to Android (touchscreen) Controller Converter

** No root required **

## Hardware
* Teensy 3.6
* Cables

## Configuring
Change the touch coordinates to the appropriate values depending on the screen size

## Usage
1. Connect controller to teensy 3.6 using host cable
1. Connect teensy 3.6 to android device using OTG cable
